# 安装homebrew
echo "==========安装homebrew=========="
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# 安装git
echo "==========安装git=========="
brew install git

# 移动到脚本所在的目录
echo "==========跳转到脚本目录=========="
cur_dir=$(cd $(dirname $0); pwd)
cd $cur_dir

# .alias
echo "==========追加.alias=========="
echo "alias ll='ls-la --color'" >> ~/.alias

# .gitconfig
echo "==========追加.gitconfig=========="
echo "[alias]
    	ck = checkout
    	lg = log --color --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit
    	drop = !git add . && git stash && git stash drop
[user]
	email = tianyalong@auto_script.com
	name = tianyalong
" >> ~/.gitconfig

echo "==========覆盖.tmux.conf=========="
mv .tmux.conf ~/
echo "==========覆盖.vimrc=========="
mv .vimrc ~/
echo "==========覆盖proxy_script=========="
mv proxy_script ~/proxy_scrip	
chmod +x proxy_script/set_proxy.sh
chmod +x proxy_script/remove_proxy.sh

# 安装zsh
echo "==========安装zsh=========="
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

echo "==========配置proxy_script到PATH=========="
echo 'export PATH=$PATH:~/proxy_scrip/' >> ~/.zshrc
source ~/.zshrc

# 安装tmux
echo "==========安装tmux=========="
brew install tmux

# 安装golang
echo "==========安装golang=========="
brew install golang
echo "==========配置golang国内代理=========="
go env -w GOPROXY=https://goproxy.cn,direct

# 安装python及miniforge
echo "==========安装miniforge并初始化zsh=========="
brew install miniforge
conda init zsh
