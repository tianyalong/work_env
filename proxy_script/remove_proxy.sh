#!/bin/bash

adb shell settings put global http_proxy :0
adb shell settings delete global http_proxy
adb shell settings delete global global_http_proxy_host
adb shell settings delete global global_http_proxy_port
