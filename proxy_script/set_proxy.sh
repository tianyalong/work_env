#!/bin/bash

echo "删除当前代理 开始"
/Users/bytedance/go/bin/remove_proxy.sh
echo "删除当前代理 结束"

ip=$1

if  [ ! -n "$1" ] ;then
    ip=`ifconfig en0 | grep 'inet ' | sed 's/^.inet //g' | sed 's/ *netmask.*$//g'`
fi


echo "代理为 $ip:8888"

adb shell settings put global http_proxy "$ip:8888"
